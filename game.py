from random import randint
from this import d
monthstring=["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]
name = input("Hello, what is your name? ")
maxguesses = 20
datelow = 1924.0
datehigh = 2004.12
date = 1000
for guess in range(maxguesses):
    # int((date-int(date))*10)
    while date <= datelow or date >= datehigh:
        month = randint(0, 11)
        year = randint(1924, 2004)
        date = year+(month/10)
   #  print("test low: ", datelow, "test high: ",datehigh,"date: ",date)
    print("Guess ",guess+1," : ", name, " , were you born on ", monthstring[month], " / ",year," ?")
    response = input("(yes, earlier or later): ")
    if response == "later":
        if guess == maxguesses-1:
            print("I have better things to do.  BYE!")
            exit()
        print("Drat! Lemme try again!")
        datelow=date

    elif response == "earlier":
        if guess == maxguesses-1:
            print("I have better things to do.  BYE!")
            exit()
        print("Drat! Lemme try again!")
        datehigh=date

    elif response == "yes":
        print("I knew it!")
        exit()
    else:
        print("Please keep your answer concise and precise: yes, earlier or later.")
        input("Let's try again, ok? okk??: ")

